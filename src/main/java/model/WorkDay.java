package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "workdays")
@Embeddable
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class WorkDay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private LocalDate date;
    private String name;
    @Column(name = "order_name")
    private String orderName;
    private int duration;
    private String detail;
}
