import lombok.Data;

import lombok.NoArgsConstructor;
import model.WorkDay;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.TypedQuery;
import java.io.*;

import java.text.ParseException;

import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;


@Data
@NoArgsConstructor
public class ParseExcel {
    private String fileName;
    private TreeSet<String> employees;
    private ArrayList<WorkDay> workDays;
    //private boolean isCheck;
    private LocalDate checkDate;
    //private int sheetNumber = 0;
    private XSSFWorkbook workbook;
    private String[] sheets;
    public String status = "";
    public String dateRange = "";

    public ParseExcel(String fileName) throws IOException {
        this.fileName = fileName;
        employees = new TreeSet<>();
        workDays = new ArrayList<>();

        FileInputStream file = new FileInputStream(fileName);
        workbook = new XSSFWorkbook(file);
        int countSheets = workbook.getNumberOfSheets();
        sheets = new String[countSheets];

        for (int i = 0; i < countSheets; i++) {
            //sheetNumber++;
            sheets[i] = workbook.getSheetName(i);
        }

    }

    public void readFromExcel() {
        int count = 0;
        String sheetName = "";

        try {
            if (checkDate())
                for (String element : sheets) {
                    count = 0;
                    sheetName = element;
                    XSSFSheet sheet = workbook.getSheet(element);

                    for (Row cells : sheet) {
                        count++;

                        if (cells.getRowNum() == 0) continue;
                        if (cells.getCell(0) == null) {
                            status = status.concat("Ошибка в строке: " + count + ", Лист: " + sheetName);
                            break;
                        }
                        if ((cells.getCell(0).getCellType() == 1) &&
                                cells.getCell(0).getStringCellValue().equals("_")) {
                            break;
                        }
                        Cell cellDate = cells.getCell(0);
                        Cell cellLastName = cells.getCell(4);
                        Cell cellFirstName = cells.getCell(5);
                        Cell cellMiddleName = cells.getCell(6);
                        Cell cellDetail = cells.getCell(12);
                        Cell cellDuration = cells.getCell(10);
                        if (cellDuration.getCellType() == Cell.CELL_TYPE_BLANK) continue;

                        String employee = addToEmployees(lastNameToLowerCase(cellLastName),
                                cellFirstName.getStringCellValue(),
                                cellMiddleName.getStringCellValue());

                        addToWorkDays(employee,
                                cellDate,
                                element,
                                cellDuration,
                                cellDetail);
                    }
                }

            List<WorkDay> sorted = new ArrayList<>(workDays);

            sorted.sort(
                    Comparator
                            .comparing(WorkDay::getDate)
                            //.reversed()
                            .thenComparing(WorkDay::getDate)

            );

            dateRange = sorted.get(0).getDate() + " - " +
                    sorted.get(sorted.size() - 1).getDate();

        } catch (Exception e) {
            //System.out.println("Ошибка в строке: " + count + " страница " + sheetName);
            status = "Ошибка в строке: " + count + ", Лист: " + sheetName;
            e.printStackTrace();
        }
    }


    public boolean checkDate() {
        XSSFSheet sheet = workbook.getSheet(sheets[0]);
        Row row = sheet.getRow(1);
        Cell cellDate = row.getCell(0);
        if (cellDate.getCellType() == 0) {
            double doubleDateFromCell = cellDate.getNumericCellValue();
            Date date = DateUtil.getJavaDate(doubleDateFromCell);
            checkDate = date.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            checkDate = LocalDate.parse(cellDate.getStringCellValue(), formatter);
        }

        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml").build();
        Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
        SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
        Session session = sessionFactory.openSession();

        String hql = "FROM WorkDay";
        TypedQuery<WorkDay> query = session.createQuery(hql, WorkDay.class);
        List<WorkDay> workDays = query.getResultList();
        if (workDays.size() > 0) {
            for (WorkDay wd : workDays) {
                if (checkDate.isBefore(wd.getDate())) {
                    //isCheck = false; // данные такие уже есть, isCheck = true и выход, isCheck заставит проверить дату ещё раз
                    status = "данные данные были загружены ранее";
                    return false;
                }
            }
        }

        return true;
    }

//    public void readFromExcel(Boolean isCheck) {
//        this.isCheck = isCheck;
//        readFromExcel();
//    }

    public LocalDate getCheckDate() {
        return checkDate;
    }

    private String lastNameToLowerCase(Cell cellLastName) {
        return cellLastName.getStringCellValue().substring(0, 1).concat(
                cellLastName.getStringCellValue().substring(1).toLowerCase(Locale.ROOT)
        );
    }

    private String addToEmployees(String lastName, String firstName, String middleName) {
        String name = lastName + " " + firstName + " " + middleName;
        employees.add(name);
        return name;
    }

    private void addToWorkDays(String employee, Cell cellDate, String order, Cell cellDuration, Cell cellDetail) throws ParseException {
        LocalDate localDate;
        if (cellDate.getCellType() == 0) {
            double doubleDateFromCell = cellDate.getNumericCellValue();
            Date date = DateUtil.getJavaDate(doubleDateFromCell);
            localDate = date.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            localDate = LocalDate.parse(cellDate.getStringCellValue(), formatter);
        }
        int durationInMin = 0;
        if (cellDuration.getCellType() == 0) {
            double doubleDurationFromCell = cellDuration.getNumericCellValue();
            durationInMin = (int) (doubleDurationFromCell * 24 * 60);
        } else if (cellDuration.getCellType() == 1) {
            String[] fragments = cellDuration.getStringCellValue().split(":");
            String tmp4duration = "P" + "T" + fragments[0] + "H" + fragments[1] + "M";
            Duration dur = Duration.parse(tmp4duration);
            durationInMin = (int) (dur.getSeconds() / 60);
        }
        WorkDay workDay = new WorkDay();
        workDay.setDate(localDate);
        workDay.setName(employee);
        workDay.setOrderName(order);
        workDay.setDuration(durationInMin);
        workDay.setDetail(getDetail(cellDetail));
        workDays.add(workDay);
    }

    private String getDetail(Cell cell) {
        String[] timeIntervals = cell.getStringCellValue().split(" ");
        String detail = "";
        int startH = -1;
        for (String interval : timeIntervals) {
            String[] times = interval.split("-");
            String startTime = times[0];
            String[] fragments = startTime.split(":");
            String hour = fragments[0];
            if (hour.equals(".....")) {
                continue;
            }
            if (startH < 0) {
                startH = Integer.parseInt(hour);
                detail = detail.concat(interval);
                continue;
            }
            if (Integer.parseInt(hour) > startH) {
                detail = detail.concat(" ").concat(interval);
            } else break;
        }
        return detail;
    }
}
